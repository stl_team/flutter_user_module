import 'package:flutter/cupertino.dart';
import 'package:rental_app/models/get_products_list_pojo.dart';
import 'package:rental_app/network/rest_ds.dart';

abstract class GetProductsListContract {
  void onGetProductsListSuccess(GetProductsListPojo pojoData);

  void onGetProductsListError(String errorTxt);
}

//GetProductsListPresenter for call and handle api flow
class GetProductsListPresenter {
  GetProductsListContract _view;
  RestDatasource api = new RestDatasource();

  GetProductsListPresenter(this._view);

  doGetProperties(
      BuildContext getContext, String categoryId, String categoryType) async {
    try {
      await api
          .getProductsList(getContext, categoryId, categoryType)
          .then((GetProductsListPojo pojoData) {
        _view.onGetProductsListSuccess(pojoData);
      });
    } catch (error) {
      print('Error occured: $error');
      _view.onGetProductsListError(error.toString());
    }
  }
}
