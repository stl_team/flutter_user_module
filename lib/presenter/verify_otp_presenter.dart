import 'package:flutter/cupertino.dart';
import 'package:rental_app/models/verify_otp_pojo.dart';
import 'package:rental_app/network/rest_ds.dart';

abstract class VerifyOTPContract {
  void onVerifyOTPSuccess(VerifyOtpPojo verifyopt);
  void onVerifyOTPError(String errorTxt);
}

//VerifyOTPPresenter for call and handle api flow
class VerifyOTPPresenter {
  VerifyOTPContract _view;
  RestDatasource api = new RestDatasource();
  VerifyOTPPresenter(this._view);

  doVerifyOTP(BuildContext getContext,String mobileno,String country_code, String otp) async{
    try {
      await api.verifyOTP(getContext,mobileno,country_code, otp).then((VerifyOtpPojo verifyopt) {
        _view.onVerifyOTPSuccess(verifyopt);
      });
    } catch(error){
      print('Error occured: $error');
      _view.onVerifyOTPError(error.toString());
    }
  }
}