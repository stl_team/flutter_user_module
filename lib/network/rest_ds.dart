import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/all_states_list_pojo.dart';
import 'package:rental_app/models/get_otp_pojo.dart';
import 'package:rental_app/models/get_products_list_pojo.dart';
import 'package:rental_app/models/main_category_pojo.dart';
import 'package:rental_app/models/reset_password_pojo.dart';
import 'package:rental_app/models/signup_pojo.dart';
import 'package:rental_app/models/user_pojo.dart';
import 'package:rental_app/models/verify_otp_pojo.dart';
import 'package:rental_app/utils/my_constants.dart';

import 'network_util.dart';

class RestDatasource {
  BuildContext mContext;
  ProgressDialog _progressDialog;

  String TAG = "";

  //BASE Url and methods for api call
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = "";
  static final REGISTER_URL = BASE_URL + "/register";
  static final LOGIN_URL = BASE_URL + "/login";
  static final VERIFY_OTP_URL = BASE_URL + "/verifyOtp";
  static final GET_OTP_URL = BASE_URL + "/resendOtp";
  static final RESET_PASSWORD_URL = BASE_URL + "/resetPassword";
  static final GET_CATEGORY_URL = BASE_URL + "/getCategories";
  static final GET_ALL_STATES_URL = BASE_URL + "/getAllStates";
  static final GET_CITIES_URL = BASE_URL + "/getCities";
  static final GET_AREAS_URL = BASE_URL + "/getAreas";
  static final GET_PROPERTIES_URL = BASE_URL + "/getProperties";
  static final GET_FURNITURES_URL = BASE_URL + "/getFurnitures";
  static final GET_BOOKS_URL = BASE_URL + "/getBooks";
  static final GET_DECORATION_URL = BASE_URL + "/getDecoration";
  static final GET_ELECTRONICS_URL = BASE_URL + "/getElectronics";
  static final GET_ORNAMENTS_URL = BASE_URL + "/getOrnaments";
  static final GET_TOYS_URL = BASE_URL + "/getToys";
  static final GET_VEHICLES_URL = BASE_URL + "/getVehicles";
  static final GET_FASHION_URL = BASE_URL + "/getFashion";

  //Key for api call and response handle
  String KEY_success_code = "200";
  String KEY_error = "Error";
  String KEY_status = "status";
  String KEY_message = "messages";
  String KEY_data = "data";
  String KEY_token = "token";
  String KEY_id = "id";
  String KEY_first_name = "first_name";
  String KEY_last_name = "last_name";
  String KEY_email = "email";
  String KEY_mobile_no = "mobile_no";
  String KEY_country_code = "country_code";
  String KEY_otp = "otp";
  String KEY_password = "password";
  String KEY_password_confirmation = "password_confirmation";
  String KEY_profile_image = "profile_image";
  String KEY_category_id = "category_id";
  String KEY_state_id = "state_id";
  String KEY_city_id = "city_id";

  ///Login api call with parameters
  Future<UserPojo> login(BuildContext getContext, String mobile_no,
      String country_code, String password) {
    TAG = "login";
    mContext = getContext;

    initProgressDialog();

    return _netUtil.post(LOGIN_URL, body: {
      KEY_mobile_no: mobile_no,
      KEY_country_code: country_code,
      KEY_password: password,
    }).then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new UserPojo.fromJson(res);
    });
  }

  ///Signup api call with details
  Future<SignupPojo> signup(
      BuildContext getContext,
      String first_name,
      String last_name,
      String email,
      String mobile_no,
      String country_code,
      String password,
      String password_confirmation,
      File fileProfile) {
    TAG = "signup";
    mContext = getContext;

    initProgressDialog();

    return _netUtil
        .postMultiPartSignup(REGISTER_URL, fileProfile, first_name, last_name,
            email, mobile_no, country_code, password, password_confirmation)
        .then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new SignupPojo.fromJson(res);
    });
  }

  ///Verify OTP with mobile number
  Future<VerifyOtpPojo> verifyOTP(BuildContext getContext, String mobile_no,
      String country_code, String otp) {
    TAG = "verifyOTP";
    mContext = getContext;

    initProgressDialog();

    return _netUtil.post(VERIFY_OTP_URL, body: {
      KEY_mobile_no: mobile_no,
      KEY_country_code: country_code,
      KEY_otp: otp,
    }).then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new VerifyOtpPojo.fromJson(res);
    });
  }

  Future<GetOtpPojo> getOTP(
      BuildContext getContext, String mobile_no, String country_code) {
    TAG = "getOTP";
    mContext = getContext;

    initProgressDialog();

    return _netUtil.post(GET_OTP_URL, body: {
      KEY_mobile_no: mobile_no,
      KEY_country_code: country_code,
    }).then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new GetOtpPojo.fromJson(res);
    });
  }

  Future<ResetPasswordPojo> resetPassword(
      BuildContext getContext,
      String mobile_no,
      String country_code,
      String password,
      String password_confirmation) {
    TAG = "resetPassword";
    mContext = getContext;

    initProgressDialog();

    return _netUtil.post(RESET_PASSWORD_URL, body: {
      KEY_mobile_no: mobile_no,
      KEY_country_code: country_code,
      KEY_password: password,
      KEY_password_confirmation: password_confirmation,
    }).then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new ResetPasswordPojo.fromJson(res);
    });
  }

  Future<LocationsCommonPojo> getAllStates(BuildContext getContext) {
    TAG = "getAllStates";
    mContext = getContext;

    initProgressDialog();

    return _netUtil.get(GET_ALL_STATES_URL).then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new LocationsCommonPojo.fromJson(res);
    });
  }

  ///Get Cities details
  Future<LocationsCommonPojo> getCities(
      BuildContext getContext, String state_id) {
    TAG = "getCities";
    mContext = getContext;

    initProgressDialog();

    return _netUtil.post(GET_CITIES_URL, body: {
      KEY_state_id: state_id,
    }).then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new LocationsCommonPojo.fromJson(res);
    });
  }

  ///Get Areas details
  Future<LocationsCommonPojo> getAreas(
      BuildContext getContext, String city_id) {
    TAG = "getAreas";
    mContext = getContext;

    initProgressDialog();

    return _netUtil.post(GET_AREAS_URL, body: {
      KEY_city_id: city_id,
    }).then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new LocationsCommonPojo.fromJson(res);
    });
  }

  ///Get categories details
  Future<GETCategoryPojo> getCategory(BuildContext getContext) {
    TAG = "getCategory";
    mContext = getContext;

    //initProgressDialog();

    return _netUtil.get(GET_CATEGORY_URL).then((dynamic res) {
      //dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new GETCategoryPojo.fromJson(res);
    });
  }

  ///Get properties details
  Future<GetProductsListPojo> getProductsList(
      BuildContext getContext, categoryId, String categoryType) {
    TAG = "getProperties";
    mContext = getContext;

    initProgressDialog();

    String getProductUrl = "";

    if (categoryType == MyConstants.CATEGORY_TYPE_PROPERTIES) {
      getProductUrl = GET_PROPERTIES_URL;
    } else if (categoryType == MyConstants.CATEGORY_TYPE_FURNITURE) {
      getProductUrl = GET_FURNITURES_URL;
    } else if (categoryType == MyConstants.CATEGORY_TYPE_BOOKS) {
      getProductUrl = GET_BOOKS_URL;
    } else if (categoryType == MyConstants.CATEGORY_TYPE_DECORATION) {
      getProductUrl = GET_DECORATION_URL;
    } else if (categoryType == MyConstants.CATEGORY_TYPE_ELECTRONICS) {
      getProductUrl = GET_ELECTRONICS_URL;
    } else if (categoryType == MyConstants.CATEGORY_TYPE_ORNAMENTS) {
      getProductUrl = GET_ORNAMENTS_URL;
    } else if (categoryType == MyConstants.CATEGORY_TYPE_TOYS) {
      getProductUrl = GET_TOYS_URL;
    } else if (categoryType == MyConstants.CATEGORY_TYPE_FASHION) {
      getProductUrl = GET_FASHION_URL;
    } else {
      getProductUrl = GET_VEHICLES_URL;
    }

    return _netUtil.post(getProductUrl, body: {
      KEY_category_id: categoryId,
    }).then((dynamic res) {
      dismissProgressDialog();

      print(TAG + "--->" + res.toString());

      if (res == null) {
        throw new Exception(
            AppLocalizations.of(mContext).translate("msg_error_server"));
      }

      if (res[KEY_status] != KEY_success_code)
        throw new Exception(res[KEY_message]);

      return new GetProductsListPojo.fromJson(res);
    });
  }

  ///Progress dialog init and show
  void initProgressDialog() {
    //For normal dialog
    if (_progressDialog == null && mContext != null) {
      _progressDialog = new ProgressDialog(mContext,
          type: ProgressDialogType.Normal,
          isDismissible: true,
          showLogs: false);
    }

    if (_progressDialog != null) {
      dismissProgressDialog();
      _progressDialog.show();
    }
  }

  ///Progress dialog dismiss
  void dismissProgressDialog() {
    if (_progressDialog != null && _progressDialog.isShowing()) {
      _progressDialog.dismiss();
    }
  }
}
