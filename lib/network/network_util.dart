import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:rental_app/network/rest_ds.dart';

class NetworkUtil {
  // next three lines makes this class a Singleton
  static NetworkUtil _instance = new NetworkUtil.internal();

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  bool apiStatusCodeCheck(int statusCode) {
    /*if (statusCode < 200 || statusCode == 400 || json == null) {
      return false;
    }*/

    return true;
  }

  ///Get type Api call and handle response
  Future<dynamic> get(String url) {
    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (!apiStatusCodeCheck(statusCode)) {
        return null;
      }

      return _decoder.convert(res);
    });
  }

  ///Post type Api call and handle response
  Future<dynamic> post(String url, {Map headers, body, encoding}) {
    return http
        .post(url, body: body, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (!apiStatusCodeCheck(statusCode)) {
        return null;
      }

      return _decoder.convert(res);
    });
  }

  ///Post with Multipart type Api call and handle response
  Future<Map<String, dynamic>> postMultiPartSignup(
      String url,
      File imageFile,
      String first_name,
      String last_name,
      String email,
      String mobile_no,
      String country_code,
      String password,
      String password_confirmation) async {
    // Intilize the multipart request
    final imageUploadRequest = http.MultipartRequest('POST', Uri.parse(url));

    /*final mimeTypeData = lookupMimeType(imageFile.path, headerBytes: [0xFF, 0xD8]).split('/');
    // Attach the file in the request
    final file = await http.MultipartFile.fromPath(RestDatasource().KEY_profile_image, imageFile.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));*/

    imageUploadRequest.fields[RestDatasource().KEY_first_name] = first_name;
    imageUploadRequest.fields[RestDatasource().KEY_last_name] = last_name;
    imageUploadRequest.fields[RestDatasource().KEY_email] = email;
    imageUploadRequest.fields[RestDatasource().KEY_mobile_no] = mobile_no;
    imageUploadRequest.fields[RestDatasource().KEY_country_code] = country_code;
    imageUploadRequest.fields[RestDatasource().KEY_password] = password;
    imageUploadRequest.fields[RestDatasource().KEY_password_confirmation] =
        password_confirmation;

    if (imageFile != null) {
      var multipartFile = await MultipartFile.fromPath(
          RestDatasource().KEY_profile_image, imageFile.path);
      imageUploadRequest.files.add(multipartFile);
    }

    try {
      final streamedResponse = await imageUploadRequest.send();

      final response = await http.Response.fromStream(streamedResponse);

      if (!apiStatusCodeCheck(response.statusCode)) {
        return null;
      }

      final Map<String, dynamic> responseData = json.decode(response.body);

      return responseData;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
