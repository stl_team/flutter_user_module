import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rental_app/localization/app_localizations.dart';

class MyUtils {

  ///Display Toast with gravity and other properties as well
  void toastdisplay(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }


  ///Password check Pattern
  bool validatePassword(BuildContext getContext,String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{2,}$';
    RegExp regex = new RegExp(pattern);
    print(value);
    if (value.isEmpty) {
      toastdisplay(AppLocalizations.of(getContext).translate('msg_enter_password'));
      return false;
    } /*else {
      if (!regex.hasMatch(value)) {
        toastdisplay(
            AppLocalizations.of(getContext).translate('msg_validate_password'));
        return false;
      }
    }*/

    return true;
  }

  ///Email check Pattern
  bool validateEmail(BuildContext getContext,String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);

    if (value.isEmpty) {
      toastdisplay(AppLocalizations.of(getContext).translate('msg_enter_email'));
      return false;
    } else {
      if (!regex.hasMatch(value)) {
        toastdisplay(AppLocalizations.of(getContext).translate('msg_valid_email'));
        return false;
      }
    }

    return true;
  }

  ///Mobile check
  bool validateMobile(BuildContext getContext,String value) {
    // Indian Mobile number are of 10 digit only
    if (value.isEmpty) {
      toastdisplay(AppLocalizations.of(getContext).translate('msg_enter_phone'));
      return false;
    }if (value.length != 10) {
      toastdisplay(AppLocalizations.of(getContext).translate('msg_mobile_10_digit'));
      return false;
    }

    return true;
  }

  ///Check for internet
  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
}