import 'package:flutter/material.dart';

class MyConstants {
  static const kGoogleApiKey = "AIzaSyCNB0ZpPNcR7QSLX9kLcmwu8D-FwKXadYs";

  //Constant strings
  static const String my_canvas_view = "My Canvas View";

  static const double inputtext_height = 40;
  static const double layout_margin = 10;
  static const double vertical_control_space = 15;
  static const double profile_image_height_width = 100;

  static const double image_product_list_h_w = 100;
  static const double image_category_rounded = 50;

  static const String PREF_KEY_ISLOGIN = "is_login";
  static const String PREF_SELECTED_LOCATION = "selected_location";

  static const String CATEGORY_TYPE_PROPERTIES = "Properties";
  static const String CATEGORY_TYPE_FURNITURE = "Furniture";
  static const String CATEGORY_TYPE_BOOKS = "Books";
  static const String CATEGORY_TYPE_DECORATION = "Decoration";
  static const String CATEGORY_TYPE_ELECTRONICS = "Electronics";
  static const String CATEGORY_TYPE_ORNAMENTS = "Ornaments";
  static const String CATEGORY_TYPE_TOYS = "Toys";
  static const String CATEGORY_TYPE_VEHICLES = "vehicles";
  static const String CATEGORY_TYPE_FASHION = "Fashion";

  static const String CURRENCY_SIGN = "₹";

  //Same size as above
  static TextStyle textStyle_list_price_title = new TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 18,
    color: Colors.black,
  );

  //Style Product list title
  static TextStyle textStyle_list_product_title = new TextStyle(
    fontWeight: FontWeight.normal,
    fontSize: 15,
    color: Colors.black,
  );

  //Style Product list sub title
  static TextStyle textStyle_list_product_sub_title = new TextStyle(
    fontWeight: FontWeight.normal,
    fontSize: 13,
    color: Colors.black,
  );

  //Style all common text
  static TextStyle textStyle_common_text = new TextStyle(
    fontWeight: FontWeight.normal,
    fontSize: 14,
    color: Colors.black,
  );
}
