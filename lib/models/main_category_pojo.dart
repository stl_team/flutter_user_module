// To parse this JSON data, do
//
//     final mainCategory = mainCategoryFromJson(jsonString);

import 'dart:convert';

GETCategoryPojo mainCategoryFromJson(String str) => GETCategoryPojo.fromJson(json.decode(str));

String mainCategoryToJson(GETCategoryPojo data) => json.encode(data.toJson());

class GETCategoryPojo {
  String status;
  String messages;
  List<Datum> data;

  GETCategoryPojo({
    this.status,
    this.messages,
    this.data,
  });

  factory GETCategoryPojo.fromJson(Map<String, dynamic> json) => GETCategoryPojo(
    status: json["status"],
    messages: json["messages"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "messages": messages,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  int id;
  String categoryName;
  int parentId;
  DateTime createdAt;
  DateTime updatedAt;
  List<Datum> subCategories;

  Datum({
    this.id,
    this.categoryName,
    this.parentId,
    this.createdAt,
    this.updatedAt,
    this.subCategories,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    categoryName: json["category_name"],
    parentId: json["parent_id"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    subCategories: json["sub_categories"] == null ? null : List<Datum>.from(json["sub_categories"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_name": categoryName,
    "parent_id": parentId,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "sub_categories": subCategories == null ? null : List<dynamic>.from(subCategories.map((x) => x.toJson())),
  };
}
