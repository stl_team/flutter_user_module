
// To parse this JSON data, do
//
//     final UserPojo = responseLoginFromJson(jsonString);

import 'dart:convert';

UserPojo responseLoginFromJson(String str) => UserPojo.fromJson(json.decode(str));

String responseLoginToJson(UserPojo data) => json.encode(data.toJson());

class UserPojo {
  String status;
  String messages;
  Data data;

  UserPojo({
    this.status,
    this.messages,
    this.data,
  });

  factory UserPojo.fromJson(Map<String, dynamic> json) => UserPojo(
    status: json["status"],
    messages: json["messages"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "messages": messages,
    "data": data.toJson(),
  };
}

class Data {
  int id;
  String firstName;
  String lastName;
  String email;
  String mobileNo;
  int otp;
  String profileImage;
  String fileExtension;
  String url;
  DateTime createdAt;
  DateTime updatedAt;
  String token;
  List<Token> tokens;

  Data({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.mobileNo,
    this.otp,
    this.profileImage,
    this.fileExtension,
    this.url,
    this.createdAt,
    this.updatedAt,
    this.token,
    this.tokens,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    email: json["email"],
    mobileNo: json["mobile_no"],
    otp: json["otp"],
    profileImage: json["profile_image"],
    fileExtension: json["file_extension"],
    url: json["url"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    token: json["token"],
    tokens: List<Token>.from(json["tokens"].map((x) => Token.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "first_name": firstName,
    "last_name": lastName,
    "email": email,
    "mobile_no": mobileNo,
    "otp": otp,
    "profile_image": profileImage,
    "file_extension": fileExtension,
    "url": url,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "token": token,
    "tokens": List<dynamic>.from(tokens.map((x) => x.toJson())),
  };
}

class Token {
  String id;
  int userId;
  int clientId;
  Name name;
  List<dynamic> scopes;
  bool revoked;
  DateTime createdAt;
  DateTime updatedAt;
  DateTime expiresAt;

  Token({
    this.id,
    this.userId,
    this.clientId,
    this.name,
    this.scopes,
    this.revoked,
    this.createdAt,
    this.updatedAt,
    this.expiresAt,
  });

  factory Token.fromJson(Map<String, dynamic> json) => Token(
    id: json["id"],
    userId: json["user_id"],
    clientId: json["client_id"],
    name: nameValues.map[json["name"]],
    scopes: List<dynamic>.from(json["scopes"].map((x) => x)),
    revoked: json["revoked"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    expiresAt: DateTime.parse(json["expires_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "client_id": clientId,
    "name": nameValues.reverse[name],
    "scopes": List<dynamic>.from(scopes.map((x) => x)),
    "revoked": revoked,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "expires_at": expiresAt.toIso8601String(),
  };
}

enum Name { MY_APP }

final nameValues = EnumValues({
  "MyApp": Name.MY_APP
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
