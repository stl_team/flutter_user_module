import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/get_products_list_pojo.dart';
import 'package:rental_app/presenter/get_products_list_presenter.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';

class ProductsList extends StatefulWidget {
  final String categoryId;
  final String title;
  final String categoryType;

  ProductsList({Key key, this.categoryId, this.title, this.categoryType})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _ProductsListStateFul();
  }
}

class _ProductsListStateFul extends State<ProductsList>
    implements GetProductsListContract {
  GetProductsListPresenter _getProductsListPresenter;
  GetProductsListPojo _getProductsListPojo;

  @override
  void initState() {
    _apiCallForGetProducts();
    super.initState();
  }

  void _apiCallForGetProducts() {
    // Check for internet
    MyUtils().check().then((intenet) {
      if (intenet != null && intenet) {
        _getProductsListPresenter = new GetProductsListPresenter(this);
        _getProductsListPresenter.doGetProperties(
            context, widget.categoryId, widget.categoryType);
      } else {
        MyUtils().toastdisplay(
            AppLocalizations.of(context).translate('msg_no_internet'));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: AutoSizeText(
          widget.title,
          maxLines: 1,
        ),
      ),
      body: _getProductsListPojo == null
          ? new Center(
              child: new CircularProgressIndicator(),
            )
          : loadProductsListData(),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    super.dispose();
  }

  ListView loadProductsListData() {
    return new ListView.builder(
      itemCount: _getProductsListPojo.data.length,
      itemBuilder: (context, position) {
        return Card(
          child: Padding(
            padding: const EdgeInsets.all(MyConstants.layout_margin),
            child: InkWell(
              onTap: () {},
              child: Container(
                height: MyConstants.image_product_list_h_w,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: new BorderRadius.circular(5.0),
                      child: FadeInImage.assetNetwork(
                        fit: BoxFit.cover,
                        placeholder: "graphics/no_product.png",
                        image:
                            (_getProductsListPojo.data[position].images != null
                                ? _getProductsListPojo.data[position].images
                                : ""),
                        height: MyConstants.image_product_list_h_w,
                        width: MyConstants.image_product_list_h_w,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(5.0),
                    ),
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  MyConstants.CURRENCY_SIGN +
                                      "" +
                                      _getProductsListPojo.data[position].price,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: MyConstants.textStyle_list_price_title,
                                ),
                              ),
                              InkWell(
                                onTap: () {},
                                child: Icon(
                                  Icons.favorite,
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),
                          Text(
                            _getProductsListPojo.data[position].adTitle,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: MyConstants.textStyle_list_product_title,
                          ),
                          Text(
                            _getProductsListPojo.data[position].description,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: MyConstants.textStyle_list_product_title,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  void onGetProductsListError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  @override
  void onGetProductsListSuccess(pojoData) {
    if (pojoData != null && pojoData.data.length > 0 && this.mounted) {
      setState(() {
        _getProductsListPojo = pojoData;
      });
    } else {
      MyUtils().toastdisplay(
          AppLocalizations.of(context).translate("msg_no_data_found"));
      Navigator.pop(context);
    }
  }
}
