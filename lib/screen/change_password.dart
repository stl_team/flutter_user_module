import 'package:flutter/material.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';

class ChangePassword extends StatefulWidget {
  ChangePassword({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _ChangePasswordStateful();
  }
}

class _ChangePasswordStateful extends State<ChangePassword> {
  //TextFiled controller for handle input event
  final textOldPasswordController = TextEditingController();
  final textPasswordController = TextEditingController();
  final textConfirmPasswordController = TextEditingController();

  final FocusNode _old_passwordFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirm_passwordFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new Center(
        child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(
                left: MyConstants.layout_margin,
                right: MyConstants.layout_margin),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  controller: textOldPasswordController,
                  obscureText: true,
                  textInputAction: TextInputAction.next,
                  focusNode: _old_passwordFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(
                        context, _old_passwordFocus, _passwordFocus);
                  },
                  decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)
                        .translate('hint_old_password'),
                  ),
                ),
                TextFormField(
                  controller: textPasswordController,
                  obscureText: true,
                  textInputAction: TextInputAction.next,
                  focusNode: _passwordFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(
                        context, _passwordFocus, _confirm_passwordFocus);
                  },
                  decoration: InputDecoration(
                    hintText:
                        AppLocalizations.of(context).translate('hint_password'),
                  ),
                ),
                TextFormField(
                  controller: textConfirmPasswordController,
                  obscureText: true,
                  textInputAction: TextInputAction.done,
                  focusNode: _confirm_passwordFocus,
                  onFieldSubmitted: (term) {
                    _submitClickValidation();
                  },
                  decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)
                        .translate('hint_confirm_password'),
                  ),
                ),

                //Todo for manage space bewtween control
                new Container(
                  margin: new EdgeInsets.fromLTRB(
                      0.0,
                      MyConstants.vertical_control_space,
                      0.0,
                      MyConstants.vertical_control_space),
                ),

                RaisedButton(
                  onPressed: () {
                    _submitClickValidation();
                  },
                  child: Text(
                      AppLocalizations.of(context).translate("label_submit")),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _submitClickValidation() {
    setState(() {
      if (!MyUtils()
          .validatePassword(context, textOldPasswordController.text)) {
        return;
      }

      if (!MyUtils().validatePassword(context, textPasswordController.text)) {
        return;
      }

      if (!MyUtils()
          .validatePassword(context, textConfirmPasswordController.text)) {
        return;
      }

      if (textOldPasswordController.text == textPasswordController.text) {
        MyUtils().toastdisplay(AppLocalizations.of(context)
            .translate('msg_notsame_od_password_new'));
        return;
      }

      //Password & Confirm password same
      if (textPasswordController.text != textConfirmPasswordController.text) {
        MyUtils().toastdisplay(AppLocalizations.of(context)
            .translate('msg_same_password_confirm'));
        return;
      }
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    textOldPasswordController.dispose();
    textPasswordController.dispose();
    textConfirmPasswordController.dispose();
    super.dispose();
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
