import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/get_otp_pojo.dart';
import 'package:rental_app/presenter/get_otp_presenter.dart';
import 'package:rental_app/screen/reset_password.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';

class ForgotPassword extends StatefulWidget {
  final String title;

  const ForgotPassword({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _ForgotPasswordStateFul();
  }
}

class _ForgotPasswordStateFul extends State<ForgotPassword> implements GetOTPContract{
  GetOTPPresenter _getOTPPresenter;
  //TextFiled controller for handle input event
  final _textPhoneController = TextEditingController();
  String selectedCountryCode="+91";

  @override
  void initState() {
    _getOTPPresenter = new GetOTPPresenter(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(
                left: MyConstants.layout_margin,
                right: MyConstants.layout_margin),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new CountryCodePicker(
                      onChanged: _onCountryChange,
                      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                      initialSelection: 'IN',
                      favorite: [selectedCountryCode,'IN'],
                      // optional. Shows only country name and flag
                      showCountryOnly: false,
                      // optional. Shows only country name and flag when popup is closed.
                      // optional. aligns the flag and the Text left
                      alignLeft: false,

                    ),

                    new Flexible(
                      child: new  TextFormField(
                        controller: _textPhoneController,
                        textInputAction: TextInputAction.done,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            hintText: AppLocalizations.of(context).translate("hint_phone_number"),
                            contentPadding: const EdgeInsets.all(10.0)),
                        onFieldSubmitted: (term) {},
                      ),
                    ),
                  ],
                ),
                //Todo for manage space bewtween control
                new Container(
                  margin: new EdgeInsets.fromLTRB(0.0, MyConstants.vertical_control_space, 0.0, MyConstants.vertical_control_space),
                ),
                RaisedButton(
                  onPressed: () {
                    _submitClickValidation();
                  },
                  child: Text(AppLocalizations.of(context).translate("label_submit")),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }


  void _submitClickValidation() {
    setState(() {
      if(!MyUtils().validateMobile(context,_textPhoneController.text)){
        return;
      }
      // Check for internet
      MyUtils().check().then((intenet) {
        if (intenet != null && intenet) {
          _getOTPPresenter.doGetOTP(
              context, _textPhoneController.text,selectedCountryCode);
        } else {
          MyUtils().toastdisplay(AppLocalizations.of(context)
              .translate('msg_no_internet'));
        }
      });
    });
  }

  void _onCountryChange(CountryCode countryCode) {
    //Todo : manipulate the selected country code here
    selectedCountryCode = countryCode.toString();
    print("New Country selected: " + countryCode.toString());
  }

  @override
  void onGetOTPError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  @override
  void onGetOTPSuccess(GetOtpPojo pojoData) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => ResetPassword(getMobileNo: selectedCountryCode+_textPhoneController.text),
        ));
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _textPhoneController.dispose();
    super.dispose();
  }
}
