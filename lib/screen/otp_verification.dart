import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/get_otp_pojo.dart';
import 'package:rental_app/models/verify_otp_pojo.dart';
import 'package:rental_app/presenter/get_otp_presenter.dart';
import 'package:rental_app/presenter/verify_otp_presenter.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';
import 'package:rental_app/utils/share_preference.dart' as mypreference;

import 'home.dart';

class OtpPage extends StatefulWidget {
  final String getMobileNo;
  final String selectedCountryCode;

  const OtpPage({Key key, this.getMobileNo, this.selectedCountryCode}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return new _OtpPageStateFul();
  }
}

class _OtpPageStateFul extends State<OtpPage> implements VerifyOTPContract, GetOTPContract {
  VerifyOTPPresenter _verifyOTPPresenter;
  GetOTPPresenter _getOTPPresenter;
  //TextFiled controller for handle input event
  final textPhoneController = TextEditingController();
  final textOTPController = TextEditingController();

  @override
  void initState() {
    //Presenter initialize for api call
    _verifyOTPPresenter = new VerifyOTPPresenter(this);
    _getOTPPresenter = new GetOTPPresenter(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    textPhoneController.text = widget.getMobileNo;

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate("label_otp_verification")),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(
                left: MyConstants.layout_margin,
                right: MyConstants.layout_margin),
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: textPhoneController,
                  enabled: false,
                  textInputAction: TextInputAction.done,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      hintText: AppLocalizations.of(context).translate("hint_phone_number"),
                      contentPadding: const EdgeInsets.all(10.0)),
                  onFieldSubmitted: (term) {},
                ),
                TextFormField(
                  controller: textOTPController,
                  textInputAction: TextInputAction.done,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      hintText: AppLocalizations.of(context).translate("hint_otp"),
                      contentPadding: const EdgeInsets.all(10.0)),
                  onFieldSubmitted: (term) {},
                ),
                //Todo for manage space bewtween control
                new Container(
                  margin: new EdgeInsets.fromLTRB(0.0, MyConstants.vertical_control_space, 0.0, MyConstants.vertical_control_space),
                ),
                Padding(
                  padding: EdgeInsets.all(MyConstants.vertical_control_space),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          // Check for internet
                          MyUtils().check().then((intenet) {
                            if (intenet != null && intenet) {
                              _getOTPPresenter.doGetOTP(
                                  context, textPhoneController.text,widget.selectedCountryCode);
                            } else {
                              MyUtils().toastdisplay(AppLocalizations.of(context)
                                  .translate('msg_no_internet'));
                            }
                          });
                        },
                        child: Text(AppLocalizations.of(context)
                            .translate("label_get_otp")),
                      ),
                      RaisedButton(
                        onPressed: () {
                          _verifyOTPClickValidation();
                        },
                        child: Text(AppLocalizations.of(context)
                            .translate("label_verify_otp")),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //Button click validation like OTP enter or not
  void _verifyOTPClickValidation() {
    setState(() {
      if(!MyUtils().validateMobile(context,textPhoneController.text)){
        return;
      }
      // Check for internet
      MyUtils().check().then((intenet) {
        if (intenet != null && intenet) {
          _verifyOTPPresenter.doVerifyOTP(
              context, textPhoneController.text,widget.selectedCountryCode, textOTPController.text);
        } else {
          MyUtils().toastdisplay(AppLocalizations.of(context)
              .translate('msg_no_internet'));
        }
      });
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    textPhoneController.dispose();
    textOTPController.dispose();
    super.dispose();
  }

  ///Handle Api error flow
  @override
  void onVerifyOTPError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  ///Handle api success flow
  @override
  void onVerifyOTPSuccess(VerifyOtpPojo verifyopt) {
    mypreference.MySharePreference()
        .saveBoolInPref(MyConstants.PREF_KEY_ISLOGIN, true);

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => MyHomePage(title: "")),
          (Route<dynamic> route) => false,
    );
  }

  ///Handle Api error flow
  @override
  void onGetOTPError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  ///Handle api success flow
  @override
  void onGetOTPSuccess(GetOtpPojo pojoData) {
    textOTPController.text = pojoData.data.otp.toString();
  }
}
