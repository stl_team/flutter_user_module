import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rental_app/localization/app_localizations.dart';
import 'package:rental_app/models/user_pojo.dart';
import 'package:rental_app/presenter/login_presenter.dart';
import 'package:rental_app/screen/change_password.dart';
import 'package:rental_app/screen/forgot_password.dart';
import 'package:rental_app/screen/home.dart';
import 'package:rental_app/screen/signup.dart';
import 'package:rental_app/utils/my_constants.dart';
import 'package:rental_app/utils/my_utils.dart';
import 'package:rental_app/utils/share_preference.dart' as mypreference;

class MyLogin extends StatefulWidget {
  MyLogin({Key key, this.title}) : super(key: key);

  final String title;
  String getUserNam = "Welcome to Login Screen";

  @override
  _MyLoginPageState createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLogin> implements LoginContract {
  LoginPresenter _loginPresenter;
  //TextFiled controller for handle input event
  String selectedCountryCode = "+91";
  final textPhoneController = TextEditingController();
  final textPasswordController = TextEditingController();

  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  bool isLoggedIn = false;

  @override
  void initState() {
    //Initialize presenter for Login Api call
    _loginPresenter = new LoginPresenter(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(AppLocalizations.of(context).translate("label_login")),
        ),
        body: new Center(
          //Single screelview for handle small screen UI
          child: SingleChildScrollView(
            child: new Container(
              constraints: const BoxConstraints(minWidth: double.infinity),
              alignment: Alignment.center,
              margin: const EdgeInsets.only(
                  left: MyConstants.layout_margin,
                  right: MyConstants.layout_margin),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: MyConstants.inputtext_height),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new CountryCodePicker(
                        onChanged: _onCountryChange,
                        // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                        initialSelection: 'IN',
                        favorite: [selectedCountryCode, 'IN'],
                        // optional. Shows only country name and flag
                        showCountryOnly: false,
                        // optional. Shows only country name and flag when popup is closed.
                        // optional. aligns the flag and the Text left
                        alignLeft: false,
                      ),
                      new Flexible(
                        child: new TextFormField(
                          controller: textPhoneController,
                          textInputAction: TextInputAction.next,
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                          keyboardType: TextInputType.number,
                          focusNode: _phoneFocus,
                          onFieldSubmitted: (term) {
                            _fieldFocusChange(
                                context, _phoneFocus, _passwordFocus);
                          },
                          decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)
                                .translate('hint_phone_number'),
                          ),
                        ),
                      ),
                    ],
                  ),
                  TextFormField(
                    controller: textPasswordController,
                    obscureText: true,
                    textInputAction: TextInputAction.done,
                    focusNode: _passwordFocus,
                    onFieldSubmitted: (term) {
                      loginCheck();
                    },
                    decoration: InputDecoration(
                      hintText: AppLocalizations.of(context)
                          .translate('hint_password'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(MyConstants.vertical_control_space),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            loginCheck();
                          },
                          child: Text(AppLocalizations.of(context)
                              .translate("label_login")),
                        ),
                        RaisedButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MySignUp(
                                      title: AppLocalizations.of(context)
                                          .translate("label_signup"))),
                            );
                          },
                          child: Text(AppLocalizations.of(context)
                              .translate("label_signup")),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                        child: Text(
                          AppLocalizations.of(context)
                              .translate('label_forgot_password'),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ForgotPassword(
                                    title: AppLocalizations.of(context)
                                        .translate("label_forgot_password"))),
                          );
                          // do what you need to do when "Click here" gets clicked
                        }),
                  ),
                  Align(
                    //Change password label with style
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                        child: Text(
                          AppLocalizations.of(context)
                              .translate('label_change_password'),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangePassword(
                                    title: AppLocalizations.of(context)
                                        .translate("label_change_password"))),
                          );
                          // do what you need to do when "Click here" gets clicked
                        }),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  //Handle TextFormField focus change handle
  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  //Visible login button on focus change
  void onLoginStatusChanged(bool isLoggedIn) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
    });
  }

  //Handle login button flow
  void loginCheck() {
    setState(() {
      //Check phone
      if (!MyUtils().validateMobile(context, textPhoneController.text)) {
        return;
      }

      //Check password
      if (!MyUtils().validatePassword(context, textPasswordController.text)) {
        return;
      }

      // Check for internet
      MyUtils().check().then((intenet) {
        if (intenet != null && intenet) {
          _loginPresenter.doLogin(
              context,textPhoneController.text,selectedCountryCode, textPasswordController.text);
        } else {
          MyUtils().toastdisplay(AppLocalizations.of(context)
              .translate('msg_no_internet'));
        }
      });
    });
  }

  //Handle country drop down value change here
  void _onCountryChange(CountryCode countryCode) {
    selectedCountryCode = countryCode.toString();
    print("New Country selected: " + countryCode.toString());
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    textPhoneController.dispose();
    textPasswordController.dispose();
    super.dispose();
  }

  //Handle Login error flow
  @override
  void onLoginError(String errorTxt) {
    MyUtils().toastdisplay(errorTxt);
  }

  //Handle Login success flow
  @override
  void onLoginSuccess(UserPojo user) {
    //Set value in prefernce for handle is login or not
    mypreference.MySharePreference()
        .saveBoolInPref(MyConstants.PREF_KEY_ISLOGIN, true);

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => MyHomePage(title: "")),
    );
  }
}
